# Pokedex 98

A simple customizable pokedex web application with some extra sections, based on the Pokémon Yellow interface design, using VueJS and the PokeAPI. Developed by Palemomos from [Themes By Pale (or Palemomos Themes)](https://themesbypale.tumblr.com/). 

> Version 1.0

## 🌸 Install this as a page in Tumblr

1. To install this app as a page in Tumblr using the source code, go to [this link](https://gitlab.com/Palemona/pokedex-98/-/raw/main/index.html), you'll be redirected to a page containing the source code.

2. Copy ALL the content of the page. Use <kbd>Ctrl</kbd> + <kbd>A</kbd> or <kbd>Command ⌘</kbd> + <kbd>A</kbd> to select all the text, and then copy it (<kbd>Ctrl</kbd> + <kbd>C</kbd> or <kbd>Command ⌘</kbd> + <kbd>C</kbd>).

3. Go to your blog and find the **Edit appereance** option. You'll be redirected you to the customization page.

4. Scroll down and find the option **Add a page**, select the "Custom Layout" option and then paste the code copied in step 2 <kbd>Ctrl</kbd> + <kbd>V</kbd> or <kbd>Command ⌘</kbd> + <kbd>V</kbd>.

5. Edit the desired information, use the **Customization Guide** listed below. You can also edit the code using a proper code editor like *Sublime Text*, *Atom*, or *VS Code*, instead of edit the code directly in the Tumblr editor, if you do this repeat step 4 after finishin editing.  

5. Click **Update Preview** and then click **Save**. Note if the page is not well displayed on the preview.

**IMPORTANT:** Make sure you have **Javascript enabled* on your blog. If you don't know how to do this, please read the following guide.


--------

## 🌸 How the Pokédex works

The  retrieves all the pokémon information through the services of the [Poke API](https://pokeapi.co/). The Poke API is a full RESTful API linked to an extensive database detailing everything about the Pokémon main game series. 

This means the app does not have storaged any information of the pokémon, instead every time you search a Pokémon the app connects to the API and gets the information back, this includes the sprites of the pokémon. Also when a user first enters the page, all the pokemon name list for the pokédex is retrieved, and the team is set too. 

## 🌸 Customization Guide

Every section that has customizable content is wrapped or preceded with a comment starting with the ⭐ symbol. To find any customizable section, find in the code all the occurrences of "⭐". 

### Appearence

To change the general appearence of the page, search in the code the lines with the following texts to customize the images and colors of the light and dark version respectively:

`/*⭐START: here change default color and background for LIGHT VERSION ⭐*/ `

`/*⭐START: here change default color and background for DARK VERSION ⭐*/`

And edit the values of the properties between START and END.
It is not neccesary to change anything directly on the CSS unless you want to change the backgrounds behaviour.

### Set the language of the pokedex

You can change the language of the pokémon's descriptions. Find the line with `⭐ Here set your language`and change the value between " ". Acceptable values are "en" (English), "es" (Spanish), "de" (German), "fr" (French), "ja-Hrkt" (Japanese), "ko" (Korean), "it" (Italian), "zh-Hans" (Chinese).

**Note:** The only text that will be changed are the description text and the name of the current pokémon, and the later will be shown in the selected language hovering the cursor over the English name.

### Set your player name

To change the name that will be displayed on the menu, find the line with `⭐ Here set your username ` and replace the value between " ".

### Set your team

To set your team, find the line with `⭐ START Here add your team.` 

You will see the following code :

```
const team = [
{ id: 151, lv: 99 }, 
{ id: 183, lv: 25 },
{ id: 6, lv: 70 },
{ id: 810, lv: 25 },
{ id: 94, lv: 40 },
{ id: 25, lv: 56 }
];
```

Each line between { } corresponds to a pokemon, so you just have to updated the **"id"** value that represents the pokémon number, and the **"lv"** value that represents the level of the pokemon. 

If you want to have less than 6 pokémon on your team, delete the remaining code lines. If you add more than 6 lines, the team will automatically take the first 6 lines.
**IMPORTANT: Level has to be a number between 0 and 100.**

### Set a song

To add a song to play in background, find in the code the line with the `<!--⭐ START here add your song source ⭐-->`, and in the src attribute put the path or url of your file.

### Change the content of the sections.

The editable sections (excluding POKéMON because that is edited in a different way) are USERNAME (an about page), ITEM (a tags o links directory) and SAVE (an extra information section). To edit each correspond section, find the lines with `⭐ START here ABOUT`, `⭐ START here ITEM` and `⭐ START here SAVE`. Each section has comments properly indicating which lines you have to edit.


---
## 🌸 Other notes

-

--------
## 🌸 Terms of Use

- You can't remove credits (visible credits or in code credits).
- You can't copy portions of the code into any other works*.
- You can't redistribute these works on other blogs or websites.
- You can't use the themes outside Tumblr or try to replicate them in other platform.
- You can use pages in other websites.
- You can customize anything (of the code) as you want*.

